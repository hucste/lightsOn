#!/bin/bash
# lightsOn.sh
set -x

# Copyright (c) 2013 iye.cba at gmail com
# url: https://github.com/iye/lightsOn
# This script is licensed under GNU GPL version 2.0 or above

# Modified by Stephane HUC
# year: 2015 > 2016
# url: https://github.com/hucste/lightsOn
# email: devs@stephane-huc.net

# Description: Bash script that prevents the screensaver and display power
# management (DPMS) to be activated when you are watching Flash Videos
# fullscreen on Firefox and Chromium.
# Can detect mplayer, minitube, and VLC when they are fullscreen too.
# Also, screensaver can be prevented when certain specified programs are running.
# lightsOn.sh needs xscreensaver or kscreensaver to work.


# HOW TO USE: Start the script with the number of seconds you want the checks
# for fullscreen to be done. Example:
# "./lightsOn.sh 120 &" will Check every 120 seconds if Mplayer, Minitube
# VLC, Firefox or Chromium are fullscreen and delay screensaver and Power Management if so.
# You want the number of seconds to be ~10 seconds less than the time it takes
# your screensaver or Power Management to activate.
# If you don't pass an argument, the checks are done every 50 seconds.
#
# An optional array variable exists here to add the names of programs that will delay the screensaver if they're running.
# This can be useful if you want to maintain a view of the program from a distance, like a music playlist for DJing,
# or if the screensaver eats up CPU that chops into any background processes you have running,
# such as realtime music programs like Ardour in MIDI keyboard mode.
# If you use this feature, make sure you use the name of the binary of the program (which may exist, for instance, in /usr/bin).
clear

# Modify these variables if you want this script to detect if Mplayer,
# VLC, Minitube, or Firefox or Chromium Flash Video are Fullscreen and disable
# xscreensaver/kscreensaver and PowerManagement.

declare -a APPS=("chromium-browser" "firefox" "google-chrome" "opera")
APPS=("${APPS[@]}" "gnome-mplayer" "mplayer" "mplayer2" "mpv" "smplayer" "totem" "vlc")
APPS=("${APPS[@]}" "minitube" "popcorn-time" "skype" "smtube")
# for plugins
APPS=("${APPS[@]}" "operapluginwrapper-native" "plugin-container")

# Names of programs which, when running, you wish to delay the screensaver.
declare -a delay_progs=() # For example ('ardour2' 'gmpc')

screensaver=""  # to set the used screensaver; DONT TOUCH!
# Screensavers Names Software
declare -a screensavers=("cinnamon-screensaver" "gnome-screensaver" "kscreensaver" "xautolock" "xscreensaver")

# YOU SHOULD NOT NEED TO MODIFY ANYTHING BELOW THIS LINE
delay="$1"
displays=""

LOCKFILE="/var/run/lock/$(basename "$0")" ;
pwd="$(dirname "$(readlink -f "$0")")"

function active_screensaver() {

    case "${screensaver}" in
        "cinnamon-screensaver") cinnamon-screensaver-command --activate > /dev/null ;;
        "gnome-screensaver") gnome-screensaver-command --activate > /dev/null ;;
        "kscreensaver") qdbus org.kde.ksmserver /ScreenSaver SetActive false > /dev/null ;;
        "xautolock")
            xautolock -enable
        ;;
        "xscreensaver") xscreensaver-command -activate > /dev/null ;;
    esac

    # Activate DPMS is on.
    xset dpms
    #xset s on

    }

function check_app_run() {
    # check if active windows is mplayer, vlc or firefox...

    #Get title of active window
    activ_win_id=$(xprop -id "${activ_win_id}")

    activ_win_pid="$(grep "_NET_WM_PID(CARDINAL)" <<< "${activ_win_id}")"
    activ_win_pid=${activ_win_pid##* }

    app_name="$(awk '{print tolower($0)}' <<< $(basename $(lsof -aFn -p ${activ_win_pid} -d txt | sed -ne 's/^n//p')))"

    if in_array "${app_name}" "${APPS[@]}"; then return 0; else return 1; fi

    }


function check_delay_progs() {

    for prog in "${delay_progs[@]}"; do

        if [[ $(pidof "${prog}") -ge 1 ]]; then

            echo "Delaying the screensaver because a program on the delay list, '${prog}', is running..."
            delay_screensaver
            break

        fi

    done

}

function check_fullscreen() {

    # loop through every display looking for a fullscreen window
    for display in $displays; do

        #get id of active window and clean output
        activ_win_id="$(DISPLAY=:0.${display} xprop -root _NET_ACTIVE_WINDOW)"
        #activ_win_id=${activ_win_id#*# } #gives error if xprop returns extra ", 0x0" (happens on some distros)
        activ_win_id="${activ_win_id:40:9}"

        if [[ -n "${activ_win_id}" ]]; then

            if check_app_run; then delay_screensaver; else active_screensaver; fi

        fi

    done

}

function delay_screensaver() {

    # reset inactivity time counter so screensaver is not started
    case "${screensaver}" in
        "cinnamon-screensaver") cinnamon-screensaver-command --deactivate > /dev/null ;;
        "gnome-screensaver") gnome-screensaver-command --deactivate > /dev/null ;;
        "kscreensaver") qdbus org.freedesktop.ScreenSaver /ScreenSaver SimulateUserActivity > /dev/null ;;
        "xautolock") xautolock -disable ;;
        "xscreensaver") xscreensaver-command -deactivate > /dev/null ;;
    esac

    #Check if DPMS is on. If it is, deactivate and reactivate again. If it is not, do nothing.
    dpms_status="$(xset -q | grep -ce 'DPMS is Enabled')"
    if [[ ${dpms_status} -eq 1 ]]; then
        xset s off
        xset -dpms
    fi

}

function detect_id_displays() {

    # enumerate all the attached screens
    displays=$(xvinfo | awk -F'#' '/^screen/ {print $2}' | xargs)

}

function detect_screensaver_used() {
    # Detect screensaver been used

    for saver in "${screensavers[@]}"; do
        if [[ -n $(pidof "${saver}") ]]; then screensaver="${saver}"; fi
    done

    if [[ -z "${screensaver}" ]]; then
        screensaver=None
        echo "No screensaver detected"
    fi

    }

function icon_into_systray() {

    # launch icon into systray
    "${pwd}"/launcher_indicator.py &

    }

function in_array() {

    # equivalent to PHP in_array
    # call: in_array needle array

    local i=0 needle="$1" IFS=" "; shift; read -a array <<< "$@"

    while [ $i -le ${#array[@]} ]; do
        if [[ "${array[$i]}" == "${needle}" ]]; then return 0; fi # true
        let i=i+1
    done
    return 1

    unset i needle IFS array

}

function manage_pid() {

    pid="${$}"

    if [[ -e "${LOCKFILE}" ]]; then
        if [[ ! -d /proc/$(cat "${LOCKFILE}") ]]; then
            rm "${LOCKFILE}"
            stop
        else
            pid_lf=$(<"${LOCKFILE}")
        fi

        if [[ ${pid} && ${pid_lf} ]]; then
            if [[ "${delay}" == "stop" ]]; then pid=${pid_lf}; fi
            stop
        fi
    else
        echo "${pid}" > "${LOCKFILE}"
    fi

    }

function start() {

    manage_pid
    test_delay

    icon_into_systray

    detect_id_displays
    detect_screensaver_used

    while true; do
        check_delay_progs
        check_fullscreen
        sleep "${delay}"
    done

    }

function stop() {

    kill -9 "${pid}"
    if [[ -e "${LOCKFILE}" ]]; then rm "${LOCKFILE}"; fi

    exit 0

    }

function test_delay() {

    # If argument empty, use 50 seconds as default.
    if [ -z "${delay}" ]; then delay=50; fi

    if [[ ${delay} = *[^0-9]* ]]; then
        # If argument is not integer quit.
        echo "The Argument '${delay}' is not valid, not an integer"
        echo "Please use the time in seconds you want the checks to repeat."
        echo "You want it to be ~10 seconds less than the time it takes your screensaver or DPMS to activate"
        exit 1
    fi

}

start
